//The Main Class is defined
public class Hello
{
// A public string is then created
  public static void main (String[]args)
  {
// The information put within the string is then output.
    System.out.println ("Hello!");
  }
}

/* This code can be compiled and tested
@: https://www.compilejava.net/
*/
