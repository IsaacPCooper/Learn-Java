public class Char {
 public static void main(String[] args) {
  /* Char's must be surrounded by ' tags
    The system can output the char Variable.
 */
  char myChar = 'B';
  System.out.println(myChar);
 }
}

/* This code can be compiled and tested
@: https://www.compilejava.net/
*/
