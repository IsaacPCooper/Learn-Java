# Learn Java
<img src="https://sdtimes.com/wp-content/uploads/2019/03/jW4dnFtA_400x400.jpg"  height="48" width="48"
     alt="Java Logo"/>
     
<b>Basic Java code to assist in the development of Java Development Knowledge</b>

<i>All code in this Repo is FREE TO USE, and may be taken and developed in any way that is needed, all releases will detail different ways of using the Java Language.</i>

<i>Please feel free to sponsor and fork the project if the work developed within this repo assists you in any capacity, any donations are greatly appreciated.</i>
